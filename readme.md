# Dockerizing Laravel

A Docker PHP development environment for running Laravel Apps on Docker. Inspired from [Laradock](https://laradock.io/)

# Licence
MIT